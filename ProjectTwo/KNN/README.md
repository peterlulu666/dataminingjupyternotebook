- Problem: Predict Titanic Survivors by applying ML techniques
- Approach: Employ the K-nearest neighbours classifier (KNN) algorithm to model and predict
- learned: Find the best K using elbow method, plot the Error rate vs. K-value
